<?php declare(strict_types = 1);

require_once "vendor/autoload.php";

$params = (new \Piotrbe\LastSha\Github\Parser\InputReader())->read($argv);
$urlComponent = $params[0];
$serviceType = isset($params[1]['service']) ? $params[1]['service'] : "";
$client = (new \Piotrbe\LastSha\Factory\ClientFactory())->create($serviceType);
$res = $client->fetchCommits($urlComponent);
/**
 * @var $pars Piotrbe\LastSha\Github\Parser\ResultParser
 */
$pars = (new \Piotrbe\LastSha\Factory\ParserFactory())->create($serviceType);
var_dump($pars->read($res));



