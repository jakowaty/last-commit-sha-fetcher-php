<?php declare(strict_types=1);

namespace Piotrbe\LastSha;

interface Client
{
    public function fetchCommits(string $oenerRepo): array;
}
