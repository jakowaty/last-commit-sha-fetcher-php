<?php declare(strict_types=1);

namespace Piotrbe\LastSha\Github\Client;

use Piotrbe\LastSha\Client;

class LastShaClient implements Client
{
    public function fetchCommits(string $oenerRepo): array
    {
        $url = "https://api.github.com/repos/" . $oenerRepo . "/commits";
        $opts = array(
            'http'=>array(
                'method'=>"GET",
                'header'=>"User-Agent: app\r\n"
            )
        );

        $json = \file_get_contents($url,false, \stream_context_create($opts));

        if (\is_string($json)) {
            return \json_decode($json, true);
        }

        return [];
    }
}
