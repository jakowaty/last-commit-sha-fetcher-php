<?php declare(strict_types=1);

namespace Piotrbe\LastSha\Github\Parser;

use Piotrbe\LastSha\Input\InputParser;
use Piotrbe\LastSha\Parser;

class ResultParser implements Parser
{
    public function read(array $arr): array
    {
        if (empty($arr)) {
            return [];
        }

        if (!isset($arr[0]["sha"])) {
            return [];
        }

        return [$arr[0]["sha"]];
    }
}
