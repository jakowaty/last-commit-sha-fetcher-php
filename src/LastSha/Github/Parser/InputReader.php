<?php declare(strict_types=1);

namespace Piotrbe\LastSha\Github\Parser;

use Piotrbe\LastSha\Input\InputParser;
use Piotrbe\LastSha\Parser;

class InputReader implements Parser
{
    public function read(array $arr): array
    {
        $result = [];
        $count = \count($arr);
        //no validation assuming int only a demo

        if ($count >= 2) {
            $result[] = $arr[\count($arr) - 1];
        }

        if (\count($arr) > 2) {
            $result[] = \getopt("s:", ["service:"]);
        }

        return $result;
    }

}
