<?php declare(strict_types=1);

namespace Piotrbe\LastSha\Factory;

use Piotrbe\LastSha\Client;
use Piotrbe\LastSha\Factory;
use Piotrbe\LastSha\Github\Client\LastShaClient;

class ClientFactory implements Factory
{
    public function create(?string $arg): Client
    {
        if (null !== $arg) {
            $arg = \mb_strtolower($arg);
        }

        switch ($arg) {
            case "":
            case null:
            case 'github':
                return new LastShaClient();
                break;

            default:
                throw new \Exception("Not implemented");
        }
    }

}
