<?php declare(strict_types=1);

namespace Piotrbe\LastSha\Factory;

use Piotrbe\LastSha\Client;
use Piotrbe\LastSha\Factory;
use Piotrbe\LastSha\Github\Parser\ResultParser;
use Piotrbe\LastSha\Parser;

class ParserFactory implements Factory
{
    public function create(?string $arg): Parser
    {
        if (null !== $arg) {
            $arg = \mb_strtolower($arg);
        }

        switch ($arg) {
            case "":
            case null:
            case 'github':
                return new ResultParser();
                break;

            default:
                throw new \Exception("Not implemented");
        }
    }

}
