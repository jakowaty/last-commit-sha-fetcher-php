<?php

namespace Piotrbe\LastSha;

interface Factory
{
    public function create(?string $arg);
}
