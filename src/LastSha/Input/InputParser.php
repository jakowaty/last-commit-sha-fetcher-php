<?php declare(strict_types=1);

namespace Piotrbe\LastSha\Input;

use Piotrbe\LastSha\Parser;

interface InputParser extends Parser {
    public function read(array $arr): array;
}
