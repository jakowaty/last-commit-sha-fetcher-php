How to test?
1. install docker-compose   
2. enter main folder with docker-compose.yml file    
3. type in console: docker-compose up   
4. type in console: docker ps   
5. remember and find id of php container (first column at left)   
6. type: docker exec -it {remembered id from 5} bash, you should be in container   
7. cd work   
8. php application.php [OPTIONS like --service] [argument owner/repo]   

to finish: write unit tests, however code
is written with unit testing in mind (mostly);
